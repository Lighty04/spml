from data_parser import DataParser
from stock_predictor_gym import StockPredictorGym

def __main__():
    train_data, test_data = DataParser("bitcoin_2016_2019.json")
    gym = StockPredictorGym(train_data)

if __name__ == "__main__": __main__()
